call pathogen#infect()
syntax enable
set nocompatible
set mouse=a
set ts=2
set sw=2
set expandtab
set wildmode=list:longest
set list listchars=tab:»\ ,trail:·
set pastetoggle=<F2>
set ignorecase
colorscheme molokai
nnoremap <F2> :NERDTreeToggle<CR>
nnoremap <F5> :bprev<CR>
nnoremap <F6> :bnext<CR>
set ruler
"set relativenumber
